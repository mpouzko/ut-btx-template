<?php
require_once __DIR__ . '/ImageResize.php';
/*
    Для начала будем считать, что у нас все работает в глобальной области видимости, поэтому переменная $APPLICATION доступна
    В случае, если реализация статических методов новозомжна - необходимо реализовать методы экземпляра класса и  и добавить конструктор

    
*/

class BitrixData
{

    /*public function __construct()
    {
        include_once __DIR__ . '/../../bitrix/modules/main/include/prolog_before.php';
        CModule::IncludeModule("iblock");
        CModule::IncludeModule("catalog");
        CModule::IncludeModule("sale");
        CModule::IncludeModule("im");
    }*/

    protected static function getSiteName($id)
    {
        $rsSites = CSite::GetByID($id);
        $arSite = $rsSites->Fetch();
        return $arSite['SITE_NAME'];
    }

    public static function getHomePageData()
    {
        $rawHomeData = CIBlockElement::GetList([], ['ID' => 45253], false, false, ['PROPERTY_HOME_SLOGAN', 'PROPERTY_HOME_BACKGROUND']);
        $homeData = $rawHomeData->Fetch();
        $resizedFileName = str_replace('/', '', CFile::GetPath($homeData['PROPERTY_HOME_BACKGROUND_VALUE']));
        $resizedFullPath = '/upload/resize_cache/resizedImages/' . $resizedFileName;
        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $resizedFullPath)) {
            $resizedImage = new \Gumlet\ImageResize($_SERVER['DOCUMENT_ROOT']. CFile::GetPath($homeData['PROPERTY_HOME_BACKGROUND_VALUE']));
            //$resizedImage->resizeToWidth(800);
            $resizedImage->quality_jpg = 80;
            $resizedImage->save($_SERVER['DOCUMENT_ROOT'] . $resizedFullPath);
        }
        return ['slogan' => $homeData['PROPERTY_HOME_SLOGAN_VALUE'], 'background' => 'https://' . $_SERVER['SERVER_NAME'] . $resizedFullPath];
    }

    /**
     *
     * Возвращает данные инфоблоков
     *
     * @return array (
     *
     * "site_title"        // Наименование сайта
     * "categoryTitle",    // Название инфоблока
     * "categoryImage"   // Изображение инфоблока
     * "seo_description"   // seo-поле description
     * "seo_keywords"      // seo-поле keywords
     * "content"           // Описание
     * )
     *
     */

    public static function getIblocks($code = '')
    {
        CModule::IncludeModule("iblock");
        if (!empty($code)) {
            $result = [];
            $res = CIBlock::GetList([], ['CODE' => $code]);
            $ob = $res->Fetch();
            $result['site_title'] = self::getSiteName($ob['LID']);
            $result['categoryTitle'] = $ob['NAME'];

            if (!empty($ob['PICTURE'])) {
            	$initialUrl = CFile::GetPath($ob['PICTURE']);
            	$resizedFileName = str_replace('/', '', $initialUrl);
            	$resizedFullPath = '/upload/resize_cache/resizedImages/' . $resizedFileName;
            	if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $resizedFullPath)) {
	                $resizedImage = new \Gumlet\ImageResize($_SERVER['DOCUMENT_ROOT']. $initialUrl);
	                $resizedImage->resizeToWidth(800);
	                $resizedImage->quality_jpg = 75;
	                $resizedImage->save($_SERVER['DOCUMENT_ROOT'] . $resizedFullPath);
            	}
            	$result['categoryImage'] = 'https://' . $_SERVER['SERVER_NAME'] . $resizedFullPath;
                /*$result['categoryImage'] = 'https://' . $_SERVER['SERVER_NAME'] . CFile::ResizeImageGet($ob['PICTURE'], ['width' => 800, 'height' => 600], BX_RESIZE_IMAGE_PROPORTIONAL, true)['src'];*/
            } else {
                $result['categoryImage'] = '';
            }
            $ipropValues = new \Bitrix\Iblock\InheritedProperty\IblockValues($ob['ID']);
            $seoValues = $ipropValues->getValues();
            $result['seo_description'] = $seoValues['SECTION_META_DESCRIPTION'];
            $result['seo_keywords'] = $seoValues['SECTION_META_KEYWORDS'];
            $result['content'] = $ob['DESCRIPTION'];
            return $result;
        } else {
            $result = [];
            $res = CIBlock::GetList([], [
                'TYPE' => 'catalog',
                'ACTIVE' => 'Y',
            ]);
            while ($ob = $res->Fetch()) {
                $tempRes = [];
                $tempRes['site_title'] = self::getSiteName($ob['LID']);
                $tempRes['categoryTitle'] = $ob['NAME'];
                if (!empty($ob['PICTURE'])) {
                	$initialUrl = CFile::GetPath($ob['PICTURE']);
            		$resizedFileName = str_replace('/', '', $initialUrl);
            		$resizedFullPath = '/upload/resize_cache/resizedImages/' . $resizedFileName;
            		if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $resizedFullPath)) {
		                $resizedImage = new \Gumlet\ImageResize($_SERVER['DOCUMENT_ROOT']. $initialUrl);
		                $resizedImage->resizeToWidth(800);
		                $resizedImage->quality_jpg = 75;
		                $resizedImage->save($_SERVER['DOCUMENT_ROOT'] . $resizedFullPath);
            		}
            		$tempRes['categoryImage'] = 'https://' . $_SERVER['SERVER_NAME'] . $resizedFullPath;
                    /*$tempRes['categoryImage'] = 'https://' . $_SERVER['SERVER_NAME'] . CFile::ResizeImageGet($ob['PICTURE'], ['width' => 800, 'height' => 600], BX_RESIZE_IMAGE_PROPORTIONAL, true)['src'];*/
                } else {
                    $tempRes['categoryImage'] = '';
                }
                $ipropValues = new \Bitrix\Iblock\InheritedProperty\IblockValues($ob['ID']);
                $seoValues = $ipropValues->getValues();
                $tempRes['seo_description'] = $seoValues['SECTION_META_DESCRIPTION'];
                $tempRes['seo_keywords'] = $seoValues['SECTION_META_KEYWORDS'];
                $tempRes['content'] = $ob['DESCRIPTION'];
                $result[] = $tempRes;
            }
            return $result;
        }
    }

    /**
     *
     * Возвращает данные секций инфоблока по его симв.коду
     *
     * @var string iblockCode    //символьный код  инфоблока
     * @return array (
     * "name",             // Название секции
     * "site_title"        // Наименование сайта
     * "categoryTitle",    // Название инфоблока
     * "categoryImage"   // Изображение инфоблока
     * "image",            // Изображение  = Картинка для анонса ИЛИ фото[0]
     * "seo_description"   // seo-поле description
     * "seo_keywords"      // seo-поле keywords
     * "content"           // Описание
     * )
     *
     */

    public static function getIblockSections($iblockCode)
    {
        CModule::IncludeModule("iblock");
        $result = [];
        $res = CIBlockSection::GetList([], ['IBLOCK_CODE' => $iblockCode]);
        while ($ob = $res->Fetch()) {
            $tempRes = [];
            $tempRes['name'] = $ob['NAME'];
            $iblockInfo = self::getIblocks($iblockCode);
            $tempRes['site_title'] = $iblockInfo['site_title'];
            $tempRes['categoryTitle'] = $iblockInfo['categoryTitle'];
            $tempRes['categoryImage'] = $iblockInfo['categoryImage'];
            if (!empty($ob['PICTURE'])) {
            		$initialUrl = CFile::GetPath($ob['PICTURE']);
            		$resizedFileName = str_replace('/', '', $initialUrl);
            		$resizedFullPath = '/upload/resize_cache/resizedImages/' . $resizedFileName;
            		if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $resizedFullPath)) {
		                $resizedImage = new \Gumlet\ImageResize($_SERVER['DOCUMENT_ROOT']. $initialUrl);
		                $resizedImage->resizeToWidth(800);
		                $resizedImage->quality_jpg = 75;
		                $resizedImage->save($_SERVER['DOCUMENT_ROOT'] . $resizedFullPath);
            		}
            		$tempRes['image'] = 'https://' . $_SERVER['SERVER_NAME'] . $resizedFullPath;
                /*$tempRes['image'] = 'https://' . $_SERVER['SERVER_NAME'] . CFile::ResizeImageGet($ob['PICTURE'], ['width' => 800, 'height' => 600], BX_RESIZE_IMAGE_PROPORTIONAL, true)['src'];*/
            } elseif (!empty($ob['DETAIL_PICTURE'])) {
            	$initialUrl = CFile::GetPath($ob['DETAIL_PICTURE']);
            	$resizedFileName = str_replace('/', '', $initialUrl);
            	$resizedFullPath = '/upload/resize_cache/resizedImages/' . $resizedFileName;
            	if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $resizedFullPath)) {
		                $resizedImage = new \Gumlet\ImageResize($_SERVER['DOCUMENT_ROOT']. $initialUrl);
		                $resizedImage->resizeToWidth(800);
		                $resizedImage->quality_jpg = 75;
		                $resizedImage->save($_SERVER['DOCUMENT_ROOT'] . $resizedFullPath);
            	}
            	$tempRes['image'] = 'https://' . $_SERVER['SERVER_NAME'] . $resizedFullPath;
                /*$tempRes['image'] = 'https://' . $_SERVER['SERVER_NAME'] . CFile::ResizeImageGet($ob['DETAIL_PICTURE'], ['width' => 800, 'height' => 600], BX_RESIZE_IMAGE_PROPORTIONAL, true)['src'];*/
            } else {
                $tempRes['image'] = '';
            }
            $ipropSectionValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($ob['IBLOCK_ID'], $ob['ID']);
            $seoSectionValues = $ipropSectionValues->getValues();
            $tempRes['seo_description'] = $seoSectionValues['SECTION_META_DESCRIPTION'];
            $tempRes['seo_keywords'] = $seoSectionValues['SECTION_META_KEYWORDS'];
            $tempRes['content'] = $ob['DESCRIPTION'];
            $result[] = $tempRes;
        }
        return $result;
    }

    /**
     *
     * Возвращает данные элемента инфоблока по его симв.коду и симв.коду инфоблока
     *
     * @var string iblockCode    //символьный код элемента инфоблока
     * @var string categoryCode  // символьный код инфоблока
     * @return array (
     * "name",           // Название элемента
     * "site_title"      // Наименование сайта
     * "categoryTitle",  // Название категории
     * "categoryImage"   // Изображение инфоблока
     * "image",          // Изображение  = Картинка для анонса ИЛИ фото[0]
     * "seo_description" // seo-поле description
     * "seo_keywords"    // seo-поле keywords
     * "content"         // Описание для анонса ИЛИ Удобства и услуги
     *
     * )
     *
     */


    public static function getIblockElement($elementCode, $iblockCode)
    {
        CModule::IncludeModule("iblock");
        $arrSelect = ['ID', 'NAME', 'CODE', 'IBLOCK_ID', 'IBLOCK_CODE', 'PREVIEW_PICTURE', 'PREVIEW_TEXT', 'PROPERTY_services_desc', 'PROPERTY_photos'];
        $res = CIBlockElement::GetList([], ['CODE' => $elementCode, 'IBLOCK_CODE' => $iblockCode], false, false, $arrSelect);
        $ob = $res->Fetch();
	if (!is_array($ob['PROPERTY_PHOTOS_VALUE'])) {
	    $ob['PROPERTY_PHOTOS_VALUE'] = [ $ob['PROPERTY_PHOTOS_VALUE'] ];
	}
        $result = [];
        $result['name'] = $ob['NAME'];
        $iblockInfo = self::getIblocks($iblockCode);
        $result['site_title'] = $iblockInfo['site_title'];
        $result['categoryTitle'] = $iblockInfo['categoryTitle'];
        $result['categoryImage'] = $iblockInfo['categoryImage'];
        if (!empty($ob['PREVIEW_PICTURE'])) {
        	$initialUrl = CFile::GetPath($ob['PREVIEW_PICTURE']);
            $resizedFileName = str_replace('/', '', $initialUrl);
            $resizedFullPath = '/upload/resize_cache/resizedImages/' . $resizedFileName;
            if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $resizedFullPath)) {
		        $resizedImage = new \Gumlet\ImageResize($_SERVER['DOCUMENT_ROOT']. $initialUrl);
		        $resizedImage->resizeToWidth(800);
		        $resizedImage->quality_jpg = 75;
		        $resizedImage->save($_SERVER['DOCUMENT_ROOT'] . $resizedFullPath);
            }
            $result['image'] = 'https://' . $_SERVER['SERVER_NAME'] . $resizedFullPath;
            /*$result['image'] = 'https://' . $_SERVER['SERVER_NAME'] . CFile::ResizeImageGet($ob['PREVIEW_PICTURE'], ['width' => 800, 'height' => 600], BX_RESIZE_IMAGE_PROPORTIONAL, true)['src'];*/
        } elseif (!empty($ob['PROPERTY_PHOTOS_VALUE'][0])) {
        	$initialUrl = CFile::GetPath($ob['PROPERTY_PHOTOS_VALUE']);
            $resizedFileName = str_replace('/', '', $initialUrl);
            $resizedFullPath = '/upload/resize_cache/resizedImages/' . $resizedFileName;
            if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $resizedFullPath)) {
		        $resizedImage = new \Gumlet\ImageResize($_SERVER['DOCUMENT_ROOT']. $initialUrl);
		        $resizedImage->resizeToWidth(800);
		        $resizedImage->quality_jpg = 75;
		        $resizedImage->save($_SERVER['DOCUMENT_ROOT'] . $resizedFullPath);
            }
            $result['image'] = 'https://' . $_SERVER['SERVER_NAME'] . $resizedFullPath;
            /*$result['image'] = 'https://' . $_SERVER['SERVER_NAME'] . CFile::ResizeImageGet($ob['PROPERTY_PHOTOS_VALUE'][0], ['width' => 800, 'height' => 600], BX_RESIZE_IMAGE_PROPORTIONAL, true)['src'];*/
        } else {
            $result['image'] = '';
        }
        $ipropElementValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($ob['IBLOCK_ID'], $ob['ID']);
        $seoElementValues = $ipropElementValues->getValues();
        $result['seo_description'] = $seoElementValues['ELEMENT_META_DESCRIPTION'];
        $result['seo_keywords'] = $seoElementValues['ELEMENT_META_KEYWORDS'];
        if (!empty($ob['PREVIEW_TEXT'])) {
            $result['content'] = $ob['PREVIEW_TEXT'];
        } elseif (!empty($ob['PROPERTY_SERVICES_DESC_VALUE']['TEXT'])) {
            $result['content'] = $ob['PROPERTY_SERVICES_DESC_VALUE']['TEXT'];
        } else {
            $result['content'] = '';
        }
        return $result;
    }
}
