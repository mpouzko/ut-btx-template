<?php
require(__DIR__ . '/BitrixData.class.php');
class Builder
{
    public $__debug;
    private $_app;
    private $config;
    private $fields;

    public function __construct($mode = 'root', $params)
    {

        global $APPLICATION;
        $this->_app = $APPLICATION;
        if (!$mode) {
            $mode = 'root';
        }
        $this->config = self::getConfig($mode);
        $this->fields = [
            "title"         => $this->_app->GetProperty('title'),
            "keywords"      => $this->_app->GetProperty("keywords"),
            "description"   => $this->_app->GetProperty("description"),
        ];


        switch ($mode) {
            case 'catalog':
                $this->initCatalog($params);
                break;

            case 'category':
                $this->initCategory($params);
                break;
            case 'root':
                $this->initRoot();
                break;

            default:
                $this->init404();
                break;
        }
    }



    /**
     * 
     * Render output
     * 
     */

    public function renderTemplate()
    {
        $template = file_get_contents($this->config['assetsPath'] . $this->config['pageTpl']);
        $contentTpl = file_get_contents($this->config['tplPath'] . $this->config['contentTpl']);
	$image=false;
        if (array_key_exists("items", $this->fields)) {
            $html = '';
            $itemTpl = file_get_contents($this->config['tplPath'] . $this->config['itemTpl']);
            foreach ($this->fields['items'] as $item) {

                $image = $item['image'] ?: $item['categoryImage'];
                $name = $item['name'] ?: $item['categoryTitle'];
                $html .= strtr(
                    $itemTpl,
                    [
                        "%TITLE%" => $name,
                        "%IMAGE%" => $image,
                        "%TEXT%"  => $item['content'],
                    ]
                );
            }
            $this->fields['content'] = $html;
        }


        $content = strtr(
            $contentTpl,
            [
                "%TITLE%" => $this->fields['name'],
                "%IMAGE%" => $this->fields['image'],
                "%TEXT%" => $this->fields['content'],

            ]
        );
	if (!$image) {
	    $image = $this->fields['image'];
	}
	$imageInfo = getimagesize($image);
	$imgWidth  = $imageInfo[0];
	$imgHeight = $imageInfo[1];
	$homePageData = BitrixData::getHomePageData();
	
        $output = strtr($template, [
            '%TITLE%'    => $this->fields['site_title'] . ' : ' . $this->fields['name'],
            '%KEYWORDS%' => $this->fields['seo_keywords'],
            '%DESCRIPTION%' => $this->fields['seo_description'],
            '%CONTENT%' => $content,
	    '%IMG%'	=> $image,
	    '%URL%'	=> "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
	    '%WIDTH%'	=> $imgWidth,
	    '%HEIGHT%'	=> $imgHeight,
	    '%SLOGAN%'	=> $homePageData['slogan'],
	    '%MAIN_BACKGROUND%'	=> $homePageData['background'],
        ]);
        return $output;
    }

    /**
     * 
     * Configure output depending on mode
     * 
     */

    private static function getConfig($mode)
    {
        $config = [
            'common' => [
                'tplPath' => dirname(__DIR__) . '/_tpl/',
                'assetsPath' => $_SERVER['DOCUMENT_ROOT'] . '/assets/',

            ],
            "root" => [
                'itemTpl' => 'homeItem.tpl',
                'contentTpl' => 'home.tpl',
                'pageTpl' => 'index.html',
            ],
            "catalog" => [
                'contentTpl' => 'catalogItem.tpl',
                'pageTpl' => 'index.html',
            ],
            "category" => [
                'itemTpl' => 'categoryItem.tpl',
                'contentTpl' => 'category.tpl',
                'pageTpl' => 'index.html',
            ],
            "404" => [
                'contentTpl' => '404.tpl',
                'pageTpl' => 'index.html',
            ],
        ];
        return (array_merge($config['common'], $config[$mode]));
    }

    /**
     * 
     * Home page mode
     * 
     */
    private function initRoot()
    {

        $data = BitrixData::getIblocks();
        $this->fields['items'] = $data;
        $this->fields['name'] = $this->fields["title"];
        $this->fields['image'] = "";
        $this->fields['site_title'] = $data[0]['site_title'];
        $this->fields['seo_keywords'] = $this->fields["keywords"];
        $this->fields['seo_description'] = $this->fields["description"];
    }

    /**
     * 
     * 404 Page mode
     * 
     */

    private function init404()
    {
        $this->fields['name'] = '404 - не найдено';
        $this->fields['image'] = "";
        $this->fields['site_title'] = $this->fields['site_title'];
    }

    /**
     * 
     * Catalog Element mode
     * 
     */

    protected function initCatalog($params)
    {
        $methodParams = $this->cleanInput($params);
        $data = BitrixData::getIblockElement($methodParams['partner'], $methodParams['category']);
        foreach ($data  as $k => $v) {
            if ($v) {
                $this->fields[$k] = $v;
            }
        }

        $this->fields['site_title'] = $data['site_title'];
        $this->fields['seo_keywords'] = $this->fields['seo_keywords'] ?: $this->fields["keywords"];
        $this->fields['seo_description'] = $this->fields['seo_description'] ?: $this->fields["description"];

    }

    /**
     * 
     * Category mode
     * 
     */



    protected function initCategory($params)
    {
        $methodParams = $this->cleanInput($params);
        $data = BitrixData::getIblockSections($methodParams['category']);
        $this->fields['items'] = $data;
        $this->fields['name'] = $data[0]['categoryTitle'];
        $this->fields['image'] = $data[0]['categoryImage'];
        $this->fields['site_title'] = $data[0]['site_title'];
        $this->fields['seo_keywords'] = $this->fields['seo_keywords'] ?: $this->fields["keywords"];
        $this->fields['seo_description'] = $this->fields['seo_description'] ?: $this->fields["description"];
    }

    /**
     * 
     * Utility function to clean any user input, e.g. $_GET
     * 
     */

    protected function cleanInput($params)
    {
        return array_map(
            function ($item) {
                return strip_tags($item);
            },
            $params
        );
    }
}
