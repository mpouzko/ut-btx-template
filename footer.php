<?php
ob_end_clean();
if (class_exists('Memcached')) {
    include_once __DIR__ . '/../../../rest/Modules/Cacher/Memcacher.php';
    $cacher = new \Rest\Modules\Cacher\Memcacher('reactoutput', $_GET);
    if ($cacher->checkCacheFile()) {
        echo $cacher->readMemcache();
    } else {
        include __DIR__ . '/lib/Builder.class.php';
        $builder = new Builder($_GET['mode'], $_GET);
        $output = $builder->renderTemplate();
        $cacher->writeMemcache($output);
        echo $output;
    }
} else {
    include __DIR__ . '/lib/Builder.class.php';
    $builder = new Builder($_GET['mode'], $_GET);
    $output = $builder->renderTemplate();
    echo $output;
}
